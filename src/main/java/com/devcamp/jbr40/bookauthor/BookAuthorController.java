package com.devcamp.jbr40.bookauthor;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookAuthorController {
    @CrossOrigin
    @GetMapping("/book")
    public ArrayList<Book> getListBook(){
        ArrayList<Book> listBook = new ArrayList<>();

        Author author1 = new Author("huong", "huong@gmail.com", 'F');
        Author author2 = new Author("toan", "toan@gmail.com", 'M');
        Author author3 = new Author("An", "an@gmail.com", 'M');

        System.out.println("author1 = " + author1);
        System.out.println("author2 = " + author2);
        System.out.println("author3 = " + author3);

        Book book1 = new Book("Conan", author1, 5, 15000);
        Book book2 = new Book("Canh dong hoang", author2, 2, 1000);
        Book book3 = new Book("Doraemon", author3, 4, 8000);

        System.out.println("book1 = " + book1);
        System.out.println("book2 = " + book2);
        System.out.println("book3 = " + book3);

        listBook.add(book1);
        listBook.add(book2);
        listBook.add(book3);

        return listBook;
    }
}
